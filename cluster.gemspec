Gem::Specification.new do |s|
  s.name        = 'cluster'
  s.version     = '1.0.0'
  s.date        = '2019-03-14'
  s.summary     = "grab bag of clustering algorithms"
  s.description = "grab bag of clustering algorithms"
  s.authors     = ["Lorraine Lee"]
  s.email       = "lori@astoundingteam.com"
  s.files       = ["lib/cluster.rb"]
  s.homepage    = "https://astoundingteam.com"
  s.license       = 'MIT'
end

