require 'minitest/autorun'

require "cluster"

class ClusterTest < Minitest::Test
  def test_random
    skip
    c = Cluster.random
    assert(c)
  end

  def test_brute_force
    skip
    c = Cluster.random(7)
    b = c.brute_force
    p b
    assert(b)
  end

  def test_12_pack
    skip
    collection = Array.new(12) {|_| Cluster.random(8).brute_force}
    p collection
    assert(true)
  end

  def test_pumpy
    skip
    c = Cluster.random(128).pumpy
    p c
    p c.map(&:length)
    assert(c)
  end

  def test_outside_in
    skip
    ps = Cluster.random(50)
    c = ps.outside_in
    p c
    p c.map(&:length)
    puts "quality: #{ps.quality(c)}"
    assert(c)
  end

  def test_outside_in_2424
    skip
    ps = Cluster.random2424(50)
    c = ps.outside_in
    # puts "#{c}"
    disp_grid = Array.new(24) {|_| Array.new(24, " ")}
    c.each_with_index do |cluster, i|
      cluster.map(&:to_a).each do |x, y|
        disp_grid[x][y] = itoc(i)
      end
    end
    puts
    puts disp_grid.map {|r| r.join("")}.join("\n")
    puts
    assert(true)
  end

  def test_two
    skip
    ps = Cluster.random2424(14)
    c = ps.two
    disp_grid = Array.new(24) {|_| Array.new(24, " ")}
    c.each_with_index do |cluster, i|
      cluster.map(&:to_a).each do |x, y|
        disp_grid[x][y] = itoc(i)
      end
    end
    puts
    puts disp_grid.map {|r| r.join("")}.join("\n")
    puts "q=#{ps.quality(c)}"
    assert(true)
  end

  def test_k_means
    c = Cluster.random2424(14)
    d = Marshal.load(Marshal.dump(c))
    cc = c.two
    disp_grid = Array.new(24) {|_| Array.new(24, " ")}
    cc.each_with_index do |cluster, i|
      cluster.map(&:to_a).each do |x, y|
        disp_grid[x][y] = itoc(i)
      end
    end
    puts
    puts disp_grid.map {|r| r.join("")}.join("\n")
    puts "q=#{c.quality(cc)} (two)"
    dd = d.k_means(2)
    disp_grid = Array.new(24) {|_| Array.new(24, " ")}
    dd.each_with_index do |cluster, i|
      cluster.map(&:to_a).each do |x, y|
        disp_grid[x][y] = itoc(i)
      end
    end
    puts
    puts disp_grid.map {|r| r.join("")}.join("\n")
    puts "q=#{d.quality(dd)} (k-means, k=2)"
    assert(true)
  end

  def test_innie_vs_outie # this is the one that still fails
    skip
    puts "in test_innie_vs_outie"
    p = Cluster.random(50)
    q = Marshal.load(Marshal.dump(p))
    c = p.inside_out
    r = p.quality(c)
    d = q.outside_in
    s = p.quality(d)
    puts "innie: #{'%7.5f'%r}, outie: #{'%7.5f'%s}"
    assert(true)
  end

  def test_comparison
    skip
    c = Cluster.random(8)
    d = Marshal.load(Marshal.dump(c))
    bstart = Time.new.to_f
    b = c.brute_force
    btime = Time.new.to_f-bstart
    bq = c.quality(b)
    ostart = Time.new.to_f
    o = d.outside_in
    otime = Time.new.to_f-ostart
    oq = d.quality(o)
    puts "\nbrute_force (quality #{bq} achieved in #{'%.3f'%btime} seconds): #{b}\n\noutside_in (quality #{oq} achieved in #{'%.3f'%otime} seconds): #{o}"
    assert(true)
  end

  def test_glom
    c = Cluster.random2424(50)
    cc = c.glom
    disp_grid = Array.new(24) {|_| Array.new(24, " ")}
    cc.each_with_index do |cluster, i|
      cluster.map(&:to_a).each do |x, y|
        disp_grid[x][y] = itoc(i)
      end
    end
    puts
    puts disp_grid.map {|r| r.join("")}.join("\n")
    puts "q=#{c.quality(cc)} (agglomerative, k=2)"
    assert(true)
  end

  def test_triangle
    c = Cluster.random2424(50)
    cc = c.triangle
    disp_grid = Array.new(24) {|_| Array.new(24, " ")}
    cc.each_with_index do |cluster, i|
      cluster.map(&:to_a).each do |x, y|
        disp_grid[x][y] = itoc(i)
      end
    end
    puts
    puts disp_grid.map {|r| r.join("")}.join("\n")
    puts "q=#{c.quality(cc)} (triangle)"
    assert(true)
  end

  def itoc(i)
    table = (("0".."9").to_a+("a".."z").to_a).zip(0..).map{|let, num| [num, let]}.to_h
    table[i]
  end
end
