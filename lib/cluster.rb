require "matrix"
require "require_all"

require_all "lib/*/*.rb"

class Cluster
  def initialize(points)
    raise DomainError unless points.class == Array
    raise DomainError unless points.all? do |point|
      return false unless point.class == Array
      point.all? {|coord| coord.kind_of?(Numeric)}
    end
    @points = points.map {|point| Vector.elements(point)}
    @distances = @points.combination(2).map do |u, v|
      [[u, v], (u-v).norm]
    end
    @distances.sort_by! {|_, d| d}
    @clusters = nil
  end

  # Create a random clustering.
  def self.random(n = 8)
    new(Array.new(n) {|_| [rand(640), rand(480)]})
  end

  # Create a random clustering in which each points x or y coordinate is an
  # integer between 0 and 23 inclusive.
  def self.random2424(n = 50)
    new((0...24).to_a.product((0...24).to_a).sample(n))
  end


  # To aid in identifying clusters as the same cluster
  # Canonical ordering of clusters is defined as
  # each cluster being ordered by first coordinate, followed by
  # subsequent coordinates in turn, and clusters ordered by first coordinate
  def canonical(c = @clusters)
    c.map(&:sort).sort_by(&:first)
  end

  # Quality metric is correlation across point pairs between distance and
  # a variable whose value is 0 for point pairs in the same cluster
  # and 1 for point pairs across different clusters.
  def quality(c = @clustering)
    cluster_points = c.flatten
    # puts "relevant_points: #{cluster_points}"
    @distances.select do |pair, dist|
      (pair-cluster_points).empty?
    end.map do |uv, dist|
      # puts "uv: #{[uv, dist]}"
      [
        dist,
        uv.map{|v| c.index {|d| d.include? v}}.reduce(:!=) ? 1 : 0
      ]
    end.corr
  end
end

class Array
  def corr
    # p self
    n, sx, sy, sxy, sx2, sy2 = [0, 0, 0, 0, 0, 0]
    each do |x, y|
      # puts "#{[x, y]}"
      n += 1
      sx += x
      sy += y
      sxy += x*y
      sx2 += x*x
      sy2 += y*y
    end
    # puts "#{[n, sx, sy, sxy, sx2, sy2]}"
    # puts (n*sx2-sx*sx)*(n*sy2-sy*sy)
    (n*sxy-sx*sy)/Math.sqrt((n*sx2-sx*sx)*(n*sy2-sy*sy))
  end
end

class Vector
  def <=>(other)
    self.to_a.zip(other.to_a).map {|x, y| x <=> y}.find(&:nonzero?) || 0
  end
end
