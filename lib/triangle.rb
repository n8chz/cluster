puts "triangle.rb checking in"

class Cluster
  def triangle
    triangles = @points.uniq.combination(3).sort_by do |triad|
      d1, d2, _ = triad.combination(2).map {|p1, p2| (p1-p2).norm}.sort
      d1/d2
    end
    @clusters = []
    assigned = []
    until triangles.empty?
      p1, p2, p3 = triangles.shift
      next if ([p1, p2, p3]-assigned).empty?
      p1g = @clusters.find{|c| c.include?(p1)}
      p2g = @clusters.find{|c| c.include?(p2)}
      if p1g
        p1g << p2
      elsif p2g
        p2g << p1
      end
      @clusters.find{|c| c.include?(p3)} || @clusters << [p3]
      assigned |= [p1, p2, p3]
    end
    @clusters
  end
end
