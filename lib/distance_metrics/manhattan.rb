class Cluster
  def manhattan(u, v)
    Vector.elements(u.to_a.zip(v.to_a).map {|r, s| r-s}.sum)
  end
end
