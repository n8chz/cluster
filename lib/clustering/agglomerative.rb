# untested

class Cluster
  def glom
    clusters = @points.map {|x| [x]}
    cdist = @distances.map do |pair, dist|
      [pair.map{|p| [p]}, dist]
    end.to_h
    centroids = @points.map {|x| [[x], x]}.to_h
    q = quality(clusters)
    qmax = -1
    while clusters.count > 1
      clusters, centroids, cdist, q = next_glom([clusters, centroids, cdist, q])
      if q > qmax
        qmax = q
        best_clustering = clusters
      end
    end
    @clusters = best_clustering
  end

  def next_glom((clusters, centroids, cdist, _)) # destructive to centroids
    # find closest pair among clusters
    c1, c2 = cdist.min_by {|_, v| v}[0]
    # remove cdist entries involving either member of that pair
    new_cdist = cdist.select {|k, _| (k&[c1, c2]).empty?}
    new_clusters = clusters-[c1, c2]
    # merge those clusters
    new_cluster = c1|c2
    # calculate centroid of newly merged cluster
    new_centroid = new_cluster.reduce(:+)/new_cluster.count
    centroids.delete(c1)
    centroids.delete(c2)
    centroids[new_cluster] = new_centroid
    # add to cdist distances from new cluster's centroid to each of the other clusters
    new_clusters.each do |cluster|
      new_cdist[[cluster, new_cluster]] = (centroids[cluster]-new_centroid).norm
    end
    # add newly merged cluster to cluster list
    new_clusters << new_cluster
    new_q = quality(new_clusters)
    return [new_clusters, centroids, new_cdist, new_q]
  end
end
