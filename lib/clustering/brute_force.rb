class Cluster
  # Brute force test of all possible clusterss, including all possible
  # cluster counts. Use only with very small point sets.
  def brute_force
    n = @points.count
    raise DomainError if n > 8
    max_q = -1
    best = nil
    @points.permutation do |perm|
      (0...(2**n)).each do |part|
        p = perm.clone
        first = p.pop
        @clusters = p.each_with_object([[first]]) do |point, acc|
          (part%2).zero? ? acc.push([point]) : acc.push(acc.pop.push(point))
          part /= 2
        end
        q = quality
        # puts "quality: #{q}"
        if q > max_q
          puts "new best clusters found"
          best = @clusters
          max_q = q
        end
      end
    end
    canonical(best)
  end
end
