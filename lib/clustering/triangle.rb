puts "triangle.rb checking in"
require "pp"

class Cluster
  def triangle
    n = @points.count
    triangles = @points.uniq.combination(3).to_a
    triangles.map! do |triangle|
      list = triangle.combination(2).map do |p1, p2|
        [[p1, p2], (p1-p2).norm]
      end.sort_by {|_, dist| dist}
      p1, p2 = list[0][0]
      {
        ratio: list[1][1]/list[0][1],
        close: [p1, p2],
        far: triangle-[p1, p2]
      }
    end.sort_by!{|h| -h[:ratio]}
    pp triangles.take(20)
    @clusters = []
    assigned = []
    until assigned.count == n
      triangle = triangles.shift
      p1, p2 = triangle[:close]
      p3 = triangle[:far][0]
      triad = [p1, p2, p3]
      next if (triad-assigned).empty?
      p1g = @clusters.find{|c| c.include?(p1)}
      p2g = @clusters.find{|c| c.include?(p2)}
      if p1g
        p1g |= [p2]
      elsif p2g
        p2g |= [p1]
      else
        @clusters << [p1, p2]
      end
      @clusters.find{|c| c.include?(p3)} || @clusters << [p3]
      assigned |= [p1, p2, p3]
    end
    # pp @clusters
    @clusters
  end
end
