class Cluster
  # Push-me-pull-you approach to clusters. Alternate between iterations of
  # #outside_in and #inside_out.
  def pumpy # push me pull you
    dist_sort = @distances.clone
    clusters = []
    while dist_sort.length >= 2 do
      c1, c2 = dist_sort.shift.shift
      i1 = clusters.index {|cluster| cluster.include? c1}
      i2 = clusters.index {|cluster| cluster.include? c2}
      if i1
        if i2.nil?
          clusters[i1] << c2
        end
      else
        if i2
          clusters[i2] << c1
        else
          clusters << [c1, c2]
        end
      end
      f1, f2 = dist_sort.pop.shift
      i1 = clusters.index {|cluster| cluster.include? f1}
      i2 = clusters.index {|cluster| cluster.include? f2}
      if i1
        if i2.nil?
          clusters << [f2]
        end
      else
        if i2
          clusters << [f1]
        else
          clusters << [f1] << [f2]
        end
      end
    end
    @clusters = if dist_sort.empty?
      clusters
    else
      # TODO: put the last point wherever quality is highest
      straggler = dist_sort.shift.shift
      clusters << [] unless c.last == []
      (0...clusters.count).map do |i|
        c = clusters.clone
        c[i] << straggler
        c
      end.max_by {|clusterz| quality(clusterz)}
    end
  end
end
