class Cluster
  # Brute force routine that returns best possible partition of point set
  # into two clusters.
  def two
    (1...2**@points.count-1).map do |i|
      c = [[], []]
      @points.each do |p|
        i, cn = i.divmod(2)
        c[cn] << p
      end
      c.clone
    end.max_by do |c|
      quality(c)
      # q.to_f.nan? ? -1 : q
    end
  end
end
