class Cluster
  # Start by creating a single cluster containing the point pair of least
  # distance. Then add remaining points one-by-one, either to an existing
  # cluster or a newly created one, on a best-fit basis.
  def inside_out
    dist_sort = @distances.transpose.shift
    first_pair = dist_sort.shift
    clusters = first_pair.map {|point| [point]}
    # puts "points to process: #{@points-first_pair}"
    (@points-first_pair).sort_by do |point|
      @distances.select do |pair, _|
        pair.include? point
      end.transpose.pop.min
    end.reverse.each do |point|
      clusters << [] unless clusters.last == []
      candidates = (0...clusters.count).map do |i|
        c = Marshal.load(Marshal.dump(clusters))
        # h/t Alex Peattie https://stackoverflow.com/a/8206537/948073
        c[i] << point
        c
      end
      clusters = candidates.max_by do |cand|
        q = quality(cand)
        if q.nil?
          puts "nil quality obtained for #{cand}"
        end
        q
        # quality(cand)
      end
    end
    clusters.pop if clusters.last == []
    canonical(clusters)
  end
end
