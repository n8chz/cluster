class Cluster
  def k_means(k)
    x_min, x_max, y_min, y_max = @points.map(&:to_a).transpose.map(&:minmax).flatten
    centroids = (0...k).map do |i|
      Vector[rand(x_min..x_max), rand(y_min..y_max)]
    end
    loop do
      # group points by nearest centroid
      @clusters = @points.group_by do |point|
        centroids.min_by do |centroid|
          (centroid-point).norm
        end
      end.to_a.transpose.pop
      # move centroids to reflect clusters
      new_centroids = @clusters.map do |cluster|
        cluster.map(&:to_a).transpose.map(&:sum).map{|x| x/cluster.length}
      end.map {|a| Vector.elements(a)}
      return @clusters if (centroids&new_centroids).count == centroids.count
      centroids = new_centroids
    end
  end
end
