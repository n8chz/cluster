class Cluster
  # Start with two clusters, initially populated with one of each of the
  # point pair of greatest distance. Add remaining points to either an existing
  # cluster, or a newly-created empty cluster, on a "best fit" basis.
  def outside_in
    dist_sort = @distances.reverse.transpose.shift
    points_outside_in = dist_sort.flatten.uniq
    dist_list = points_outside_in.map do |point|
      dist_to_nearest = @distances.select do |pair, _|
        pair.include? point
      end.transpose.pop.min
      [point, dist_to_nearest]
    end.sort_by {|_, d| -d}
    # puts "#{dist_list}"
    first_pair = dist_sort.shift
    clusters = first_pair.map {|point| [point]}
    (dist_list.transpose.shift-first_pair).each do |point|
      clusters << [] unless clusters.last == []
      candidates = (0...clusters.count).map do |i|
        c = Marshal.load(Marshal.dump(clusters))
        # h/t Alex Peattie https://stackoverflow.com/a/8206537/948073
        c[i] << point
        c
      end
      # h/t Nick Cox https://stats.stackexchange.com/q/316616
      clusters = candidates.reject {|c| c.all? {|d| d.length == 1}}.max_by do |cand|
        quality(cand)
      end
    end
    clusters.pop if clusters.last == []
    canonical(clusters)
  end
end
